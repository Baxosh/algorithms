console.clear()

console.time()
function isValid(s: string): boolean {
    const temp: string[] = []
    const parentheses = {')': '(', '}': '{', ']': '['}

    for (let i = 0; i < s.length; i++) {
        const el = s[i]
        if (temp.length < 1) temp.push(el)
        else if (temp[temp.length - 1] !== parentheses[el]) temp.push(el)
        else temp.pop()
    }
    return temp.length < 1
}
console.timeEnd()

console.log(isValid("()[]{}()"))
